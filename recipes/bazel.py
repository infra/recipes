# Copyright 2023 The Pigweed Authors
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.
"""Recipe for getting Bazel and running a test with it."""

from __future__ import annotations

import re
from typing import TYPE_CHECKING

from PB.recipes.pigweed.bazel import InputProperties
from recipe_engine import post_process

if TYPE_CHECKING:  # pragma: no cover
    from typing import Generator
    from recipe_engine import recipe_api, recipe_test_api

DEPS = [
    'pigweed/bazel',
    'pigweed/checkout',
    'pigweed/ci_status',
    'pigweed/environment',
    'recipe_engine/properties',
]

PROPERTIES = InputProperties


def RunSteps(api: recipe_api.RecipeScriptApi, props: InputProperties):
    if res := api.ci_status.exit_early_in_recipe_testing_if_failing():
        return res  # pragma: no cover

    checkout: api.checkout.CheckoutContext = api.checkout(
        props.checkout_options
    )
    env: api.environment.Environment = api.environment.init(checkout)

    continue_after_build_error = False
    for change in checkout.changes:
        if 'build-errors: continue' in change.commit_message.lower():
            continue_after_build_error = True

    runner: api.bazel.BazelRunner = api.bazel.new_runner(
        checkout,
        props.bazel_options,
        continue_after_build_error=continue_after_build_error,
    )
    with env():
        runner.run()


def GenTests(api) -> Generator[recipe_test_api.TestData, None, None]:
    def properties(**kwargs):
        props = InputProperties(**kwargs)
        props.checkout_options.CopyFrom(api.checkout.git_options())
        props.bazel_options.CopyFrom(api.bazel.options())
        return api.properties(props)

    yield api.test(
        'simple',
        properties(),
        api.post_process(post_process.DropExpectation, 'checkout pigweed'),
    )

    yield api.test(
        'build-errors-continue',
        properties(),
        api.checkout.try_test_data(),
        api.checkout.cl_branch_parents(message='Build-Errors: continue'),
        api.post_process(
            post_process.StepCommandContains,
            'bazel build //...',
            ['--keep_going'],
        ),
        api.post_process(
            post_process.StepCommandContains,
            'bazel test //...',
            ['--keep_going'],
        ),
        api.post_process(post_process.DropExpectation),
    )
