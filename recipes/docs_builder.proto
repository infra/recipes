// Copyright 2020 The Pigweed Authors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License. You may obtain a copy of
// the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
// License for the specific language governing permissions and limitations under
// the License.

syntax = "proto3";

package recipes.pigweed.docs_builder;

import "recipe_modules/pigweed/bazel/options.proto";
import "recipe_modules/pigweed/build/options.proto";
import "recipe_modules/pigweed/checkout/options.proto";
import "recipe_modules/pigweed/environment/options.proto";
import "recipe_modules/pigweed/pw_presubmit/options.proto";

message InputProperties {
  // Checkout module options.
  recipe_modules.pigweed.checkout.Options checkout_options = 1;

  // Environment module options.
  recipe_modules.pigweed.environment.Options environment_options = 2;

  oneof action {
    // Build module options.
    recipe_modules.pigweed.build.Options build_options = 3;

    // Pw Presubmit module options.
    recipe_modules.pigweed.pw_presubmit.Options pw_presubmit_options = 4;

    // Bazel module options.
    recipe_modules.pigweed.bazel.Options bazel_options = 5;
  }

  // Path to the HTML root within the build directory, with forward slashes.
  string html_path = 6;

  // Destination GCS bucket. Default is "pigweed-docs".
  string bucket = 7;

  // Don't do anything that has external effects.
  bool dry_run = 8;

  // Link to webpage. Only used in the summary_result of the build. Will be
  // evaluated using '.format()'. Default: "https://pigweed.dev/?rev={}".
  string link_template = 9;

  // Whether to comment on the triggering CL with a link to the rendered docs.
  bool comment = 10;
}
