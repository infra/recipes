# Copyright 2024 The Pigweed Authors
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.
"""Recipe for testing Pigweed using presubmit_checks.py script."""

from __future__ import annotations

from typing import TYPE_CHECKING

from PB.recipes.pigweed.git_archive import InputProperties

from recipe_engine import post_process

if TYPE_CHECKING:  # pragma: no cover
    from typing import Generator
    from recipe_engine import recipe_api, recipe_test_api

DEPS = [
    'fuchsia/buildbucket_util',
    'fuchsia/git',
    'fuchsia/gsutil',
    'pigweed/checkout',
    'recipe_engine/buildbucket',
    'recipe_engine/context',
    'recipe_engine/path',
    'recipe_engine/properties',
    'recipe_engine/step',
]

PROPERTIES = InputProperties


def RunSteps(
    api: recipe_api.RecipeScriptApi,
    props: InputProperties,
) -> None:
    """Run Pigweed presubmit checks."""
    if api.buildbucket_util.is_tryjob:
        raise api.step.InfraFailure('This recipe does not support tryjobs')

    checkout = api.checkout(props.checkout_options)

    default_name = checkout.options.remote.split('/')[-1].removesuffix('.git')
    name = props.name or default_name

    tar_name = f'{name}-{api.buildbucket.build.id}-{checkout.revision()}.tar.gz'
    archive = api.path.mkdtemp('archive') / tar_name

    with api.context(cwd=checkout.root):
        cmd = [
            'archive',
            '--output',
            archive,
            '--format=tar.gz',
            'HEAD',
        ]
        api.git('archive', *cmd)

    if api.buildbucket_util.is_dev_environment:
        api.step.empty('skipping upload in dev')
    else:
        assert props.gcs_bucket
        api.gsutil.upload(bucket=props.gcs_bucket, src=archive, dst=tar_name)
        pres = api.step.empty('gs path').presentation
        pres.properties['gcs_path'] = f'gs://{props.gcs_bucket}/{tar_name}'
        pres.properties['url'] = (
            f'https://storage.googleapis.com/{props.gcs_bucket}/{tar_name}'
        )


def GenTests(api) -> Generator[recipe_test_api.TestData, None, None]:
    """Create tests."""

    yield api.test(
        'ci',
        api.properties(
            gcs_bucket='bucket-bucket-bucket',
            checkout_options=api.checkout.git_options(),
        ),
        api.checkout.ci_test_data(),
        api.post_process(
            post_process.PropertyMatchesRE,
            'gcs_path',
            r'gs://bucket-bucket-bucket/pigweed-\d+-([\da-f]*|HASH)\.tar\.gz',
        ),
        api.post_process(
            post_process.PropertyMatchesRE,
            'url',
            r'https://storage.googleapis.com/bucket-bucket-bucket/pigweed-\d+-([\da-f]*|HASH)\.tar\.gz',
        ),
        api.post_process(post_process.DropExpectation),
    )

    yield api.test(
        'dev',
        api.properties(
            gcs_bucket='bucket-bucket-bucket',
            checkout_options=api.checkout.git_options(),
        ),
        api.checkout.ci_test_data(bucket='foo.dev.ci'),
        api.post_process(post_process.MustRun, 'skipping upload in dev'),
        api.post_process(post_process.DropExpectation),
    )

    yield api.test(
        'try',
        api.properties(
            gcs_bucket='bucket-bucket-bucket',
            checkout_options=api.checkout.git_options(),
        ),
        api.checkout.try_test_data(),
        api.post_process(post_process.DropExpectation),
        status='INFRA_FAILURE',
    )
