# Copyright 2021 The Pigweed Authors
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.
"""Update the pinned versions of Python packages."""

from __future__ import annotations

import re
from typing import TYPE_CHECKING

import attr
from PB.recipes.pigweed.update_python_versions import InputProperties

if TYPE_CHECKING:  # pragma: no cover
    from typing import Generator
    from recipe_engine import recipe_test_api

DEPS = [
    'fuchsia/auto_roller',
    'pigweed/build',
    'pigweed/checkout',
    'pigweed/environment',
    'recipe_engine/properties',
    'recipe_engine/step',
]

PROPERTIES = InputProperties


def RunSteps(api, props):  # pylint: disable=invalid-name
    checkout = api.checkout(props.checkout_options)
    env = api.environment.init(
        checkout,
        props.environment_options,
        use_constraint_file=False,
    )

    with env():
        constraint_file = props.path_to_constraint_file
        if not constraint_file:
            build = api.build.create(checkout.root, props.build_options)
            api.build.gn_gen(build)

            # pw_build_PIP_CONSTRAINTS is a GN arg that points to a pip
            # constraint file. For more see
            # https://pigweed.googlesource.com/pigweed/pigweed/+/main/pw_build/python.gni
            gn_args = api.build.get_gn_args(
                build,
                test_data=[
                    {
                        'name': 'pw_build_PIP_CONSTRAINTS',
                        'current': {'value': ['//foo/bar/constraint.file']},
                    }
                ],
            )
            constraint_file = gn_args['pw_build_PIP_CONSTRAINTS']['current'][
                'value'
            ][0].strip('/')

        assert constraint_file

        # Write list of packages to constraint file.
        cmd = [
            'pw',
            'python-packages',
            'list',
            checkout.root / constraint_file,
        ]
        api.step('freeze package list', cmd)

    change = api.auto_roller.attempt_roll(
        api.auto_roller.Options(
            remote=checkout.options.remote,
            upstream_ref=checkout.options.branch,
            dry_run=props.dry_run,
            bot_commit=props.bot_commit,
        ),
        repo_dir=checkout.root,
        commit_message='roll: Update Python package versions',
    )

    return api.auto_roller.raw_result(change)


def GenTests(api) -> Generator[recipe_test_api.TestData, None, None]:
    def properties(**kwargs):
        props = InputProperties(**kwargs)
        props.checkout_options.CopyFrom(api.checkout.git_options())
        return api.properties(props)

    yield api.test('simple') + properties() + api.auto_roller.success()
