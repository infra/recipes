# Copyright 2021 The Pigweed Authors
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.
"""Recipe for testing Pigweed with multiple swarming tasks."""

from __future__ import annotations

from typing import TYPE_CHECKING

from PB.recipes.pigweed.build import InputProperties
from recipe_engine import post_process

if TYPE_CHECKING:  # pragma: no cover
    from typing import Generator
    from recipe_engine import recipe_test_api

DEPS = [
    'pigweed/build',
    'pigweed/checkout',
    'pigweed/environment',
    'recipe_engine/properties',
    'recipe_engine/step',
]

PROPERTIES = InputProperties


def RunSteps(api, props):
    checkout = api.checkout(props.checkout_options)
    env = api.environment.init(checkout, props.environment_options)
    digest = None

    with env():
        build = api.build.create(checkout.root, props.build_options)
        api.build(build)
        if not props.skip_archive:
            digest = api.build.archive_to_cas(build)

    with api.step.nest('result') as pres:
        if digest:
            pres.properties['digest'] = digest


def GenTests(api) -> Generator[recipe_test_api.TestData, None, None]:
    def properties(**kwargs):
        props = InputProperties(**kwargs)
        props.checkout_options.CopyFrom(api.checkout.git_options())
        props.build_options.CopyFrom(api.build.options(gn_args=['gnarg']))
        return api.properties(props)

    yield api.test(
        'basic',
        api.checkout.ci_test_data(),
        properties(),
        api.post_process(post_process.DropExpectation, 'checkout pigweed'),
    )

    yield api.test(
        'skip_archive',
        api.checkout.ci_test_data(),
        properties(skip_archive=True),
        api.post_process(post_process.DropExpectation, 'checkout pigweed'),
    )
