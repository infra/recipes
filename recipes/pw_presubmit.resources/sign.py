# Copyright 2020 The Pigweed Authors
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.

from __future__ import annotations

import argparse
import base64
import os

from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.serialization import load_pem_private_key

RELEASE_PKEY_PATH = "/etc/release_keys/release_key.pem"
RELEASE_PUBKEY_PATH = "/etc/release_keys/release_key_pub.pem"


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--archive-file")
    opts = parser.parse_args()

    # Validate args
    if not opts.archive_file or not os.path.exists(opts.archive_file):
        return

    if not os.path.exists(RELEASE_PKEY_PATH) or not os.path.exists(
        RELEASE_PUBKEY_PATH
    ):
        return

    # Open the private key file
    with open(RELEASE_PKEY_PATH, "rb") as f:
        pkey_data = f.read()
    private_key = load_pem_private_key(pkey_data, None, default_backend())

    # Open and sign the archive.
    with open(opts.archive_file, "rb") as f:
        archive_data = f.read()

    signature = private_key.sign(archive_data)
    print(base64.b64encode(signature).decode())


if __name__ == "__main__":
    main()
