#!/bin/bash
# Updates the pinned version of black to the latest available.
#
# Usage: $ update_black.sh

set -o errexit
set -o pipefail

function main {
  cd "$(dirname "${BASH_SOURCE[0]}")/.."

  # Look at the latest version of black, pull all the git revisions, pick the
  # one that comes first alphabetically (to minimize churn since many revisions
  # might refer to the same package).
  new_version=$(
    cipd describe "fuchsia/tools/black/\${platform}" -version latest |
    grep version |
    sort |
    head -n1)
  new_version="${new_version// /}" # Trim whitespace.

  ensure_file="$(pwd)/cipd.ensure"
  # Update pinned version in the ensure file. Assumes that the ensure file is
  # valid and holds only a single pinned package.
  in_place_sed "s/version:\S\+/$new_version/" "$ensure_file"
}

function in_place_sed {
  local directive="$1"
  local file="$2"
  # Unfortunately, it's impossible to do an in-place sed that works on both Mac
  # and Linux without creating a backup file.
  sed -i.bak "$directive" "$file"
  rm "$file.bak"
}

main "$@"
