#!/bin/bash
# Uses CIPD to install the version of black pinned in the cipd.ensure file,
# and prints the path to the black executable to stdout.
#
# Intended to be executed by other scripts that need to ensure black is
# installed.

set -o errexit
set -o pipefail

cd "$(dirname "${BASH_SOURCE[0]}")/.."

cipd_dir="$(pwd)/.cipd"
ensure_file="$(pwd)/cipd.ensure"
cipd ensure -root "$cipd_dir" -ensure-file "$ensure_file" -log-level error

echo "$cipd_dir/black"
