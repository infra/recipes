# Copyright 2021 The Pigweed Authors
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.
"""Full test of build module."""

from __future__ import annotations

from typing import TYPE_CHECKING

from PB.recipe_modules.pigweed.build.tests.full import InputProperties

if TYPE_CHECKING:  # pragma: no cover
    from typing import Generator
    from recipe_engine import recipe_test_api

DEPS = [
    'pigweed/build',
    'recipe_engine/path',
    'recipe_engine/properties',
]

PROPERTIES = InputProperties


def RunSteps(api, props):
    build = api.build.create(
        api.path.start_dir / 'checkout',
        props.build_options,
    )
    api.build(build)
    api.build.get_gn_args(build)
    api.build.archive_to_cas(build)
    api.build.download_from_cas(build, 'digest')


def GenTests(api) -> Generator[recipe_test_api.TestData, None, None]:
    yield (
        api.test('full')
        + api.properties(
            build_options=api.build.options(
                packages=['pkg'],
                gn_args=['foo=true'],
                ninja_targets=['target'],
            )
        )
    )

    yield (
        api.test('default') + api.properties(build_options=api.build.options())
    )
