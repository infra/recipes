# Copyright 2023 The Pigweed Authors
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.
"""Test API for bazel module."""

from __future__ import annotations

import re

from PB.recipe_modules.pigweed.bazel.options import BazelInvocation, Options
from recipe_engine import recipe_test_api


class BazelTestApi(recipe_test_api.RecipeTestApi):
    """Test API for Bazel."""

    def options(
        self,
        *,
        bazelisk_version='',
        cipd_json_path='',
        invocations=(('build', '//...'), ('test', '//...')),
        program=(),
    ):
        opts = Options()
        opts.cipd_json_path = cipd_json_path
        opts.bazelisk_version = bazelisk_version
        for invocation in invocations:
            invocation_proto = BazelInvocation(args=invocation)
            opts.invocations.append(invocation_proto)
        opts.program.extend([program] if isinstance(program, str) else program)
        return opts

    def properties(self, *, name='bazel_options', **kwargs):
        return self.m.properties(**{name: self.options(**kwargs)})

    def config(
        self,
        *,
        name: str = 'pigweed.json',
        programs: dict[str, list[list[str]]] | None = None,
        remote: bool = False,
        remote_cache: bool = True,
        upload_local_results: bool = True,
    ):
        if programs is None:
            programs = {
                'default': [
                    ['build', '//...'],
                    ['test', '//...'],
                ],
                'docs': [
                    ['build', '//docs'],
                ],
            }

        return self.step_data(
            f'read {name}',
            self.m.file.read_json(
                {
                    'pw': {
                        'bazel_presubmit': {
                            'programs': programs,
                            'remote': remote,
                            'remote_cache': remote_cache,
                            'upload_local_results': upload_local_results,
                        },
                    },
                },
            ),
        )
