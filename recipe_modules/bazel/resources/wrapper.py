#!/usr/bin/env python3
"""Invoke Bazel, and write a JSON file maybe pointing to ResultStore."""

import argparse
import json
import re
import subprocess
import sys


def run(*, cmd, json_path):
    # Always write this file, even if we never see a resultstore link.
    with open(json_path, 'w') as outs:
        json.dump({}, outs)

    proc = subprocess.Popen(
        cmd,
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT,
    )
    for line in proc.stdout:
        line = line.decode()
        print(line, end='')
        if match := re.search(
            r'Streaming build results to:\s*(https?://.*?)\s*$',
            line,
        ):
            with open(json_path, 'w') as outs:
                json.dump(dict(resultstore=match.group(1)), outs)
                break

    for line in proc.stdout:
        line = line.decode()
        print(line, end='')

    return proc.wait()


def parse(argv=None):
    parser = argparse.ArgumentParser()
    parser.add_argument('--json', dest='json_path', required=True)
    parser.add_argument('cmd', nargs='+')
    return vars(parser.parse_args(argv))


def main(argv=None):
    return run(**parse(argv))


if __name__ == '__main__':
    sys.exit(main())
