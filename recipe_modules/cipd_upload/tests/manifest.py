# Copyright 2024 The Pigweed Authors
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.
"""Full test of cipd_upload module."""

from __future__ import annotations

from typing import TYPE_CHECKING

from PB.recipe_modules.fuchsia.cipd_util.upload_manifest import (
    CIPDUploadManifest,
)
from PB.recipe_modules.pigweed.checkout.options import (
    Options as CheckoutOptions,
)

if TYPE_CHECKING:  # pragma: no cover
    from typing import Generator
    from recipe_engine import recipe_api, recipe_test_api

DEPS = [
    'pigweed/checkout',
    'pigweed/cipd_upload',
    'recipe_engine/file',
    'recipe_engine/path',
]


def RunSteps(api):
    manifest = CIPDUploadManifest()
    manifest.files.append(
        CIPDUploadManifest.FileToUpload(
            dest='foo.bar',
            source='bazel-out/foo.bar',
        )
    )
    manifest.pkg_name = 'pigweed/test_package/${platform}'

    manifest_path = api.path.start_dir / 'manifest.json'
    api.file.write_proto(
        f'write cipd manifest {manifest_path}',
        manifest_path,
        manifest,
        codec='JSONPB',
    )

    checkout = api.checkout(
        CheckoutOptions(
            remote='https://pigweed.googlesource.com/pigweed/pigweed.git',
        ),
        root=api.path.start_dir / 'checkout',
    )
    cas_digests: dict[str, str] = {}

    api.cipd_upload.manifest(
        manifest_path=manifest_path,
        build_dir=api.path.start_dir / 'build',
        checkout=checkout,
        cas_digests=cas_digests,
        upload_to_cipd=True,
    )


def GenTests(api) -> Generator[recipe_test_api.TestData, None, None]:
    yield api.test('manifest')
