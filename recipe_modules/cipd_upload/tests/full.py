# Copyright 2020 The Pigweed Authors
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.
"""Full test of cipd_upload module."""

from __future__ import annotations

from typing import TYPE_CHECKING

from recipe_engine import config, recipe_api

if TYPE_CHECKING:  # pragma: no cover
    from typing import Generator
    from recipe_engine import recipe_test_api

DEPS = [
    'pigweed/cipd_upload',
    'recipe_engine/path',
    'recipe_engine/platform',
    'recipe_engine/properties',
]

PROPERTIES = {
    'add_platform': recipe_api.Property(kind=bool, default=True),
    'package_paths': recipe_api.Property(kind=config.List(str), default=()),
    'package_root': recipe_api.Property(kind=str, default=None),
    'path': recipe_api.Property(kind=str, default='foo/bar/baz'),
    'repository': recipe_api.Property(
        kind=str, default='https://pigweed.googlesource.com/pigweed/pigweed'
    ),
    'search_tag_key': recipe_api.Property(kind=str, default='key'),
    'search_tag_value': recipe_api.Property(kind=str, default='value'),
}


def RunSteps(
    api,
    add_platform,
    package_paths,
    package_root,
    path,
    repository,
    search_tag_key,
    search_tag_value,
):  # pylint: disable=invalid-name
    """Run test steps."""

    package_root = api.path.start_dir / (package_root or 'pkgroot')

    metadata = [('abc', '123')]

    api.cipd_upload(
        add_platform=add_platform,
        metadata=metadata,
        paths=package_paths,
        package_root=package_root,
        cipd_path=path,
        repository=repository,
        search_tag={search_tag_key: search_tag_value},
    )


def GenTests(api) -> Generator[recipe_test_api.TestData, None, None]:
    """Define tests."""

    yield api.test(
        'found',
        api.platform('linux', 64),
        api.cipd_upload.search_results('foo/bar/baz key:value', instances=1),
    )

    yield api.test(
        'upload',
        api.platform('linux', 64),
        api.cipd_upload.search_results('foo/bar/baz key:value', instances=0),
    )

    yield api.test(
        'extra_tags',
        api.properties(extra_tags={'abc': '123'}),
        api.platform('linux', 64),
        api.cipd_upload.search_results('foo/bar/baz key:value', instances=0),
    )
