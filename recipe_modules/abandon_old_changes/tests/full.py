# Copyright 2024 The Pigweed Authors
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.
"""Full test of abandon_old_changes module."""

from __future__ import annotations

from typing import Generator, TYPE_CHECKING

from recipe_engine import post_process

if TYPE_CHECKING:  # pragma: no cover
    from recipe_engine import recipe_test_api

DEPS = [
    'fuchsia/buildbucket_util',
    'pigweed/abandon_old_changes',
]


def RunSteps(api):
    api.abandon_old_changes(host='pigweed')


def GenTests(api) -> Generator[recipe_test_api.TestData, None, None]:
    yield api.buildbucket_util.test(
        'success',
        api.post_process(
            post_process.MustRun,
            'abandon old changes.abandon 1001',
        ),
        api.post_process(
            post_process.MustRun,
            'abandon old changes.abandon 1002',
        ),
        api.post_process(
            post_process.MustRun,
            'abandon old changes.success',
        ),
        api.post_process(
            post_process.DoesNotRun,
            'abandon old changes.failure',
        ),
        api.post_process(post_process.DropExpectation),
    )

    yield api.buildbucket_util.test(
        'failure',
        api.override_step_data(
            'abandon old changes.get old changes',
            retcode=1,
        ),
        api.post_process(
            post_process.DoesNotRun,
            'abandon old changes.success',
        ),
        api.post_process(
            post_process.MustRun,
            'abandon old changes.failure',
        ),
        api.post_process(post_process.DropExpectation),
    )
