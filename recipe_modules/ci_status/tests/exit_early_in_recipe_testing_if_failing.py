# Copyright 2023 The Pigweed Authors
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.
"""Full test of ci_status.exit_early_in_recipe_testing_if_failing()."""

from __future__ import annotations

from typing import TYPE_CHECKING

from recipe_engine import post_process

if TYPE_CHECKING:  # pragma: no cover
    from typing import Generator
    from recipe_engine import recipe_test_api

DEPS = [
    'fuchsia/builder_status',
    'pigweed/checkout',
    'pigweed/ci_status',
    'recipe_engine/buildbucket',
    'recipe_engine/properties',
    'recipe_engine/step',
]


def RunSteps(api):
    res = api.ci_status.exit_early_in_recipe_testing_if_failing()
    if res:
        with api.step.nest('exit early') as pres:
            pres.step_summary_text = res.summary_markdown
    else:
        api.step('continue', None)


def GenTests(api) -> Generator[recipe_test_api.TestData, None, None]:
    def assert_continue():
        return api.post_process(post_process.MustRun, "continue")

    def assert_exit_early():
        return api.post_process(post_process.MustRun, "exit early")

    def recipe_testing_enabled():
        return api.properties(**{"$fuchsia/recipe_testing": {"enabled": True}})

    def assert_config_fetched():
        return api.post_process(
            post_process.MustRun,
            'checking CI status.fetch project cr-buildbucket.cfg',
        )

    def assert_config_not_fetched():
        return api.post_process(
            post_process.DoesNotRun,
            'checking CI status.fetch project cr-buildbucket.cfg',
        )

    def drop_expectations_must_be_last():
        # No need for expectation files, everything of note here is tested by
        # assertions. This must be the last thing added to the test.
        return api.post_process(post_process.DropExpectation)

    yield api.test(
        'not-in-recipe-testing',
        assert_continue(),
        drop_expectations_must_be_last(),
    )

    yield api.test(
        'passing',
        api.checkout.try_test_data(),
        recipe_testing_enabled(),
        assert_config_fetched(),
        assert_continue(),
        drop_expectations_must_be_last(),
    )

    yield api.test(
        'failing',
        api.checkout.try_test_data(),
        recipe_testing_enabled(),
        assert_config_fetched(),
        api.buildbucket.simulated_search_results(
            [api.builder_status.failure(), api.builder_status.failure()],
            step_name='checking CI status.buildbucket.search',
        ),
        assert_exit_early(),
        drop_expectations_must_be_last(),
    )

    yield api.test(
        'exists',
        api.properties(ci_builder_exists=True),
        api.checkout.try_test_data(),
        recipe_testing_enabled(),
        assert_continue(),
        assert_config_not_fetched(),
        drop_expectations_must_be_last(),
    )

    yield api.test(
        'does-not-exist',
        api.properties(ci_builder_exists=False),
        api.checkout.try_test_data(),
        recipe_testing_enabled(),
        assert_continue(),
        assert_config_not_fetched(),
        drop_expectations_must_be_last(),
    )
