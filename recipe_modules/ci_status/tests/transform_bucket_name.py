# Copyright 2023 The Pigweed Authors
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.
"""Test of ci_status.transform_bucket_name()."""

from __future__ import annotations

from typing import TYPE_CHECKING

from PB.recipe_modules.pigweed.ci_status.tests.transform_bucket_name import (
    InputProperties,
)
from recipe_engine import post_process

if TYPE_CHECKING:  # pragma: no cover
    from typing import Generator
    from recipe_engine import recipe_test_api

DEPS = [
    'pigweed/ci_status',
    'recipe_engine/properties',
    'recipe_engine/step',
]

PROPERTIES = InputProperties


def RunSteps(api, props):
    api.step(api.ci_status.transform_bucket_name(props.bucket_name), None)


def GenTests(api) -> Generator[recipe_test_api.TestData, None, None]:
    def test(orig, expected):
        return api.test(
            orig,
            api.properties(InputProperties(bucket_name=orig)),
            api.post_process(post_process.MustRun, expected),
            api.post_process(post_process.DropExpectation),
        )

    yield test('ci', 'ci')
    yield test('try', 'ci')
    yield test('foo.ci', 'foo.ci')
    yield test('foo.try', 'foo.ci')
    yield test('ci.foo', 'ci.foo')
    yield test('try.foo', 'ci.foo')
    yield test('shadow.ci', 'ci')
    yield test('shadow.try', 'ci')
    yield test('ci.shadow', 'ci')
    yield test('try.shadow', 'ci')
    yield test('foo.ci.shadow', 'foo.ci')
    yield test('foo.try.shadow', 'foo.ci')
    yield test('release', 'release')
    yield test('release.shadow', 'release')
