# Copyright 2021 The Pigweed Authors
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.
"""Full test of util module."""

from __future__ import annotations

import re
from typing import TYPE_CHECKING

from recipe_engine import config, recipe_api

if TYPE_CHECKING:  # pragma: no cover
    from typing import Generator
    from recipe_engine import recipe_test_api

DEPS = [
    'pigweed/util',
    'recipe_engine/buildbucket',
    'recipe_engine/file',
    'recipe_engine/path',
    'recipe_engine/properties',
    'recipe_engine/step',
]

PROPERTIES = {
    'regex': recipe_api.Property(kind=str, default=r'regex'),
    'comments': recipe_api.Property(kind=config.List(str), default=()),
}


def RunSteps(api, regex, comments):  # pylint: disable=invalid-name
    with api.step.nest('gerrit'):
        api.util.get_change_with_comments()
        if not api.util.find_matching_comment(re.compile(regex), comments):
            raise api.step.StepFailure('failure')
        api.file.write_json(
            'write metadata',
            api.path.start_dir / 'metadata.json',
            api.util.build_metadata(),
        )


def GenTests(api) -> Generator[recipe_test_api.TestData, None, None]:
    yield (
        api.test('found')
        + api.buildbucket.try_build()
        + api.properties(regex=r'foo', comments=['', 'foobar'])
        + api.util.change_comment('comment')
    )

    yield (
        api.test(
            'not_found',
            status='FAILURE',
        )
        + api.buildbucket.try_build()
        + api.properties(regex=r'foo', comments=['', 'fubar'])
    )
