# Copyright 2021 The Pigweed Authors
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.
"""Test API for pw_presubmit."""

from __future__ import annotations

from PB.recipe_modules.pigweed.gerrit_comment.options import CommentBehavior
from PB.recipe_modules.pigweed.pw_presubmit import options
from recipe_engine import recipe_test_api


class PwPresubmitTestApi(recipe_test_api.RecipeTestApi):
    """Test API for pw_presubmit."""

    def options(
        self,
        *,
        command_name='python -m pw_cli',
        program=(),
        step=(),
        only_on_changed_files=False,
        export_dir_name='export',
        continue_after_build_error=False,
        comment_allowed_gerrit_hosts=["pigweed-review.googlesource.com"],
        comment_behavior=CommentBehavior.COMMENT_UNSPECIFIED,
        use_time_for_rng_seed=False,
        override_gn_arg=(),
    ):
        opts = options.Options()
        opts.command_name = command_name
        opts.program.extend(program)
        opts.step.extend(step)
        opts.only_on_changed_files = only_on_changed_files
        opts.export_dir_name = export_dir_name
        opts.continue_after_build_error = continue_after_build_error
        opts.gerrit_comment.comment_behavior = comment_behavior
        opts.gerrit_comment.allowed_gerrit_hosts.extend(
            comment_allowed_gerrit_hosts
        )
        opts.use_time_for_rng_seed = use_time_for_rng_seed
        opts.override_gn_arg.extend(override_gn_arg)
        return opts
