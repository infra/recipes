// Copyright 2021 The Pigweed Authors
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License. You may obtain a copy of
// the License at
//
//     https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
// License for the specific language governing permissions and limitations under
// the License.

syntax = "proto3";

// This message is used for both the pw_presubmit and pw_presubmit_container
// recipes.
package recipe_modules.pigweed.pw_presubmit;

import "recipe_modules/pigweed/gerrit_comment/options.proto";

message Options {
  // Command name in case it's changed by a dependent project. Default if unset
  // is "pw".
  string command_name = 1;

  // Step to run from 'pw presubmit'. See 'pw presubmit --help' for options.
  // Default is to run all steps.
  repeated string step = 2;

  // Program to run from 'pw presubmit'. See 'pw presubmit --help' for options.
  // Default is to let 'pw presubmit' run its default program.
  repeated string program = 3;

  // Only run this check against changed files.
  bool only_on_changed_files = 4;

  // Subdirectory of the build directory to upload to GCS. Required to upload
  // build artifacts.
  string export_dir_name = 5;

  // Do not use the --full argument when only_on_changed_files is False. This is
  // only here for backwards compatibility.
  bool do_not_use_full_argument = 6;

  // Continue building after a build error.
  bool continue_after_build_error = 7;

  // Do not use the --debug-log argument.
  bool do_not_use_debug_log = 8;

  // When to post Gerrit comments (if at all).
  recipe_modules.pigweed.gerrit_comment.Options gerrit_comment = 9;

  // Whether to use the time to seed random number generators (most jobs will
  // ignore this value). Default seed is 1.
  bool use_time_for_rng_seed = 10;

  // Additional GN args to pass into presubmit steps.
  repeated string override_gn_arg = 11;
}
