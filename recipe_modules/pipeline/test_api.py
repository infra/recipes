# Copyright 2020 The Pigweed Authors
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.
"""Test API for pipeline."""

from __future__ import annotations

from recipe_engine import recipe_test_api


class EnvironmentTestApi(recipe_test_api.RecipeTestApi):
    """Test API for pipeline."""

    def props(self, round, build_ids=()):
        return self.m.properties(
            **{
                '$pigweed/pipeline': {
                    'inside_a_pipeline': True,
                    'round': round,
                    'builds_from_previous_iteration': [
                        str(x) for x in build_ids
                    ],
                },
            }
        )
