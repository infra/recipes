# Copyright 2022 The Pigweed Authors
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.
"""Full test of pipeline module."""

from __future__ import annotations

from typing import TYPE_CHECKING

from recipe_engine import post_process

if TYPE_CHECKING:  # pragma: no cover
    from typing import Generator
    from recipe_engine import recipe_test_api

DEPS = [
    'pigweed/pipeline',
    'recipe_engine/step',
]


def RunSteps(api):
    if not api.pipeline.in_pipeline:
        api.step('not in pipeline', None)
        return

    api.step('in pipeline', None)

    api.step(f'round {api.pipeline.round}', None)

    prev_builds = api.pipeline.builds_from_previous_iteration
    api.step(f'{len(prev_builds)} previous builds', None)
    for build in prev_builds:
        api.step(f'previous build {build}', None)


def GenTests(api) -> Generator[recipe_test_api.TestData, None, None]:
    def ran(x):
        return api.post_process(post_process.MustRun, x)

    def drop():
        return api.post_process(post_process.DropExpectation)

    yield api.test(
        'nopipeline',
        ran('not in pipeline'),
    )

    yield api.test(
        'round_0',
        api.pipeline.props(0, []),
        ran('in pipeline'),
        ran('round 0'),
        ran('0 previous builds'),
        drop(),
    )

    yield api.test(
        'round_1',
        api.pipeline.props(1, [123]),
        ran('in pipeline'),
        ran('round 1'),
        ran('1 previous builds'),
        ran('previous build 123'),
        drop(),
    )

    yield api.test(
        'round_4',
        api.pipeline.props(4, [123, 456, 789]),
        ran('in pipeline'),
        ran('round 4'),
        ran('3 previous builds'),
        ran('previous build 123'),
        ran('previous build 456'),
        ran('previous build 789'),
        drop(),
    )

    yield api.test(
        'empty',
        ran('not in pipeline'),
        drop(),
    )
