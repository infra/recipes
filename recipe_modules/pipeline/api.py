# Copyright 2022 The Pigweed Authors
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.
"""Calls to build code."""

from __future__ import annotations

from typing import TYPE_CHECKING

from recipe_engine import recipe_api

if TYPE_CHECKING:  # pragma: no cover
    from PB.recipe_modules.pigweed.pipeline import properties as properties_pb


class PipelineApi(recipe_api.RecipeApi):
    """Calls to build code."""

    def __init__(self, props: properties_pb.InputProperties, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._enabled: bool = props.inside_a_pipeline
        self._round: int = props.round
        self._prev_iteration_builds: tuple[str, ...] = tuple(
            props.builds_from_previous_iteration
        )

    @property
    def in_pipeline(self) -> bool:
        return self._enabled

    @property
    def round(self) -> int:
        return self._round if self._enabled else None

    @property
    def builds_from_previous_iteration(self) -> tuple[str, ...]:
        return tuple(self._prev_iteration_builds) if self._enabled else None
