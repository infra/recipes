# Copyright 2024 The Pigweed Authors
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.

from __future__ import annotations

from PB.recipe_modules.pigweed.bazel_roll.cipd_package import BazelCipdPackage
from PB.recipe_modules.pigweed.bazel_roll.tests.cipd_package import (
    CipdProperties,
)
from recipe_engine import post_process

DEPS = [
    "fuchsia/roll_commit_message",
    "pigweed/bazel_roll",
    "pigweed/checkout",
    "recipe_engine/properties",
    "recipe_engine/step",
]

PROPERTIES = CipdProperties


def RunSteps(  # pylint: disable=invalid-name
    api: recipe_api.RecipeScriptApi,
    props: CipdProperties,
):
    checkout = api.checkout.fake_context()

    assert len(props.cipd_packages) == 1

    rolls = api.bazel_roll.update_cipd_package(
        checkout=checkout,
        package=props.cipd_packages[0],
    )

    pres = api.step.empty('commit message').presentation
    pres.step_summary_text = api.roll_commit_message.format(
        *rolls,
        roll_prefix='roll:',
        send_comment=True,
    )


def GenTests(api) -> Generator[recipe_test_api.TestData, None, None]:
    """Create tests."""

    def properties(pkg: BazelCipdPackage, **kwargs):
        props = CipdProperties(**kwargs)
        props.cipd_packages.append(pkg)
        return api.properties(props)

    yield api.test(
        'success',
        properties(api.bazel_roll.cipd_package()),
        api.bazel_roll.workspace_file(
            'clang',
            api.bazel_roll.cipd_entry(
                name='llvm_toolchain',
                build_file='//pw_toolchain/build_external:llvm_clang.BUILD',
                spec='fuchsia/third_party/clang/${os}-${arch}',
                tag='git_revision:8280651ad57cb9fb24a404cec2401040c28dec98',
            ),
        ),
    )
