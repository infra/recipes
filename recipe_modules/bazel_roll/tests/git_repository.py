# Copyright 2024 The Pigweed Authors
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.

from __future__ import annotations

from PB.recipe_modules.pigweed.bazel_roll.git_repository import GitRepository
from PB.recipe_modules.pigweed.bazel_roll.tests.git_repository import (
    GitProperties,
)
from recipe_engine import post_process

DEPS = [
    "fuchsia/gitiles",
    "fuchsia/roll_commit_message",
    "pigweed/bazel_roll",
    "pigweed/checkout",
    "recipe_engine/properties",
    "recipe_engine/step",
]

PROPERTIES = GitProperties


def RunSteps(  # pylint: disable=invalid-name
    api: recipe_api.RecipeScriptApi,
    props: GitProperties,
):
    checkout = api.checkout.fake_context()

    assert len(props.git_repositories) == 1

    rolls = api.bazel_roll.update_git_repository(
        checkout=checkout,
        git_repository=props.git_repositories[0],
    )

    pres = api.step.empty('commit message').presentation
    pres.step_summary_text = api.roll_commit_message.format(
        *rolls,
        roll_prefix='roll:',
        send_comment=True,
    )


def GenTests(api) -> Generator[recipe_test_api.TestData, None, None]:
    """Create tests."""

    def _url(x: str = 'pigweed/pigweed'):
        assert ':' not in x
        return f'https://pigweed.googlesource.com/{x}'

    def trigger(url, **kwargs):
        return api.checkout.ci_test_data(git_repo=_url(url), **kwargs)

    def properties(repo: GitRepository, **kwargs):
        props = GitProperties(**kwargs)
        props.git_repositories.append(repo)
        return api.properties(props)

    workspace_file = lambda prefix: api.bazel_roll.workspace_file(
        prefix,
        api.bazel_roll.git_entry(
            comments=0,
            name='other-repo',
            remote='https://pigweed.googlesource.com/other/repo.git',
            revision="invalid commit line won't be found",
        ),
        api.bazel_roll.git_entry(
            comments=4,
            name='pigweed',
            remote='https://pigweed.googlesource.com/pigweed/pigweed.git',
            revision='1' * 40,
            strip_prefix='pw_toolchain_bazel',
        ),
        api.bazel_roll.git_entry(
            comments=0,
            name='third-repo',
            revision='2' * 40,
            remote='https://pigweed.googlesource.com/third/repo.git',
        ),
    )

    yield api.test(
        'success',
        properties(api.bazel_roll.git_repo()),
        trigger('pigweed/pigweed'),
        workspace_file('pigweed'),
        api.gitiles.log("pigweed.log pigweed", "A"),
    )

    yield api.test(
        'unrecognized-remote',
        properties(api.bazel_roll.git_repo(remote=_url('unrecognized-remote'))),
        trigger('unrecognized-remote'),
        workspace_file('unrecognized-remote'),
        api.post_process(
            post_process.MustRun,
            'unrecognized-remote.failed to update commit hash',
        ),
        status='FAILURE',
    )

    yield api.test(
        'bad-trigger',
        properties(
            api.bazel_roll.git_repo(
                remote=_url('foo'),
                workspace_path='bar/WORKSPACE',
            ),
        ),
        trigger('bar'),
        workspace_file('foo'),
        status='FAILURE',
    )

    yield api.test(
        'no-trigger',
        properties(api.bazel_roll.git_repo()),
        workspace_file('pigweed'),
        api.gitiles.log("pigweed.log pigweed", "A"),
    )

    yield api.test(
        'backwards',
        properties(api.bazel_roll.git_repo()),
        workspace_file('pigweed'),
        api.gitiles.log("pigweed.log pigweed", "A", n=0),
    )
