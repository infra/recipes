# Copyright 2023 The Pigweed Authors
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.
"""Full test of save_logs module."""

from __future__ import annotations

from typing import TYPE_CHECKING

if TYPE_CHECKING:  # pragma: no cover
    from typing import Generator
    from recipe_engine import recipe_test_api

DEPS = [
    'pigweed/save_logs',
    'recipe_engine/path',
    'recipe_engine/step',
]


def RunSteps(api):
    with api.step.nest('save logs') as pres:
        api.save_logs(
            (api.path.start_dir / 'checkout',),
            export_dir=api.path.start_dir / 'export',
            pres=pres,
            step_passed=False,
            step_name='step1',
        )


def GenTests(api) -> Generator[recipe_test_api.TestData, None, None]:
    yield api.test(
        'full',
        api.step_data('save logs.logs.foo.log', retcode=1),
        status='FAILURE',
    )
