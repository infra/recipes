# Copyright 2024 The Pigweed Authors
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.
"""Test API for submodule_roll."""

from __future__ import annotations

from PB.recipe_modules.pigweed.submodule_roll.submodule import SubmoduleEntry
from recipe_engine import recipe_test_api


class SubmoduleRollTestApi(recipe_test_api.RecipeTestApi):
    """Test API for submodule_roll."""

    def _url(self, x):
        if x.startswith(('https://', 'sso://', '.')):
            return x
        return 'https://foo.googlesource.com/' + x

    def entry(self, path, name=None, branch=None):
        return SubmoduleEntry(path=path, name=name or path, branch=branch or '')

    def gitmodules(self, prefix=None, **submodules):
        branches = {}
        for k, v in submodules.items():
            if k.endswith('_branch'):
                branches[k.replace('_branch', '')] = v

        for x in branches:
            del submodules[f'{x}_branch']

        text = []
        for k, v in submodules.items():
            text.append(
                '[submodule "{0}"]\n\tpath = {0}\n\turl = {1}\n'.format(
                    k, self._url(v)
                )
            )
            if k in branches:
                text.append(f'\tbranch = {branches[k]}\n')

        prefix = prefix.removesuffix('.') + '.' if prefix else ''

        return self.step_data(
            f'{prefix}read .gitmodules', self.m.file.read_text(''.join(text))
        )
