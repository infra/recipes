# Copyright 2024 The Pigweed Authors
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.

from __future__ import annotations

from PB.recipe_modules.pigweed.submodule_roll.tests.full import InputProperties

DEPS = [
    "fuchsia/gitiles",
    'fuchsia/roll_commit_message',
    "pigweed/checkout",
    "pigweed/submodule_roll",
    "recipe_engine/properties",
    "recipe_engine/step",
]

PROPERTIES = InputProperties


def RunSteps(  # pylint: disable=invalid-name
    api: recipe_api.RecipeScriptApi,
    props: InputProperties,
):
    checkout = api.checkout.fake_context()

    rolls = api.submodule_roll.update(checkout, props.submodule)

    if not rolls:
        with api.step.nest('nothing to roll, exiting'):
            return

    message = api.step.empty('commit message').presentation
    message.step_summary_text = api.roll_commit_message.format(
        *rolls,
        roll_prefix='roll:',
        send_comment=True,
    )


def GenTests(api) -> Generator[recipe_test_api.TestData, None, None]:
    """Create tests."""

    def properties(*submodules, **kwargs):
        props = InputProperties(**kwargs)
        assert len(submodules) == 1
        props.submodule.CopyFrom(submodules[0])
        return api.properties(props)

    yield api.test(
        'success',
        properties(api.submodule_roll.entry('a1')),
        api.submodule_roll.gitmodules(
            'a1',
            a1='sso://foo/a1',
            b2='sso://foo/b2',
        ),
        api.gitiles.log("a1.log a1", "A"),
    )

    yield api.test(
        'noop',
        properties(api.submodule_roll.entry('b2')),
        api.submodule_roll.gitmodules('b2', b2='b2', b2_branch='branch'),
        api.gitiles.log("b2.log b2", "A", n=0),
    )

    yield api.test(
        'missing',
        properties(api.submodule_roll.entry('b2')),
        api.submodule_roll.gitmodules('b2', a1='sso://foo/a1'),
        status='FAILURE',
    )
