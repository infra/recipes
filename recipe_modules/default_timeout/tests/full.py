# Copyright 2022 The Pigweed Authors
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.
"""Full test of timeout module."""

from __future__ import annotations

import datetime
from typing import TYPE_CHECKING

from PB.recipe_modules.pigweed.pw_presubmit.tests.full import InputProperties
from recipe_engine import post_process

if TYPE_CHECKING:  # pragma: no cover
    from typing import Generator
    from recipe_engine import recipe_test_api

DEPS = [
    'pigweed/checkout',
    'pigweed/default_timeout',
    'recipe_engine/step',
    'recipe_engine/time',
]

PROPERTIES = InputProperties


def RunSteps(api, props):  # pylint: disable=invalid-name
    for i in range(5):
        with api.default_timeout():
            api.step(f'step{i}', [f'step{i}'])


def GenTests(api) -> Generator[recipe_test_api.TestData, None, None]:
    """Create tests."""

    yield api.test(
        'medium',
        api.checkout.try_test_data(
            start_time=datetime.datetime.utcfromtimestamp(1600000000),
            execution_timeout=30 * 60,
        ),
        api.time.seed(1600000000),
        api.time.step(20.0),
        api.post_process(post_process.MustRun, 'step0'),
        api.post_process(post_process.MustRun, 'step1'),
        api.post_process(post_process.MustRun, 'step2'),
        api.post_process(post_process.MustRun, 'step3'),
        api.post_process(post_process.MustRun, 'step4'),
        api.post_process(post_process.DropExpectation),
    )

    # This doesn't really test anything in this module, except the test-only
    # code that ignores this module's logic when the remaining time is negative.
    yield api.test(
        'negative',
        api.checkout.try_test_data(),
        api.post_process(post_process.MustRun, 'step0'),
        api.post_process(post_process.MustRun, 'step1'),
        api.post_process(post_process.MustRun, 'step2'),
        api.post_process(post_process.MustRun, 'step3'),
        api.post_process(post_process.MustRun, 'step4'),
        api.post_process(post_process.DropExpectation),
    )

    yield api.test(
        'timed-out',
        api.checkout.try_test_data(
            start_time=datetime.datetime.utcfromtimestamp(1600000000),
            execution_timeout=15,
        ),
        api.time.seed(1600000000),
        api.time.step(1.0),
        api.post_process(post_process.MustRun, 'step0'),
        api.post_process(post_process.MustRun, 'step1'),
        api.post_process(post_process.MustRun, 'step2'),
        api.post_process(post_process.DoesNotRun, 'step3'),
        api.post_process(post_process.DoesNotRun, 'step4'),
        api.post_process(post_process.DropExpectation),
        status='FAILURE',
    )
