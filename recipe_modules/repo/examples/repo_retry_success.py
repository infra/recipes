# -*- coding: utf-8 -*-
# Copyright 2020 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

# Copied from
# https://chromium.googlesource.com/chromiumos/infra/recipes/+/HEAD/recipe_modules/repo/
# pylint: disable=missing-module-docstring

from typing import Generator

from recipe_engine import recipe_test_api

DEPS = [
    'pigweed/repo',
    'recipe_engine/path',
]


def RunSteps(api):
    api.repo.ensure_synced_checkout(
        api.path.cleanup_dir / 'ensure', 'http://manifest_url'
    )


def attempt_retry_repo(api, attempt):  # pylint: disable=invalid-name
    """Step data for retries."""
    if attempt == 1:
        step_text = 'ensure synced checkout.repo init'
        retcode = 128
    else:
        step_text = 'ensure synced checkout.clean up root path and retry'
        retcode = 0
    return api.step_data(step_text, retcode=retcode)


def GenTests(api) -> Generator[recipe_test_api.TestData, None, None]:
    yield (
        api.test('repo_retry_success')
        + attempt_retry_repo(api, 1)
        + attempt_retry_repo(api, 2)
    )
