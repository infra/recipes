# -*- coding: utf-8 -*-
# Copyright 2020 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

from typing import Generator

from recipe_engine import recipe_test_api

DEPS = [
    'pigweed/repo',
    'recipe_engine/assertions',
    'recipe_engine/context',
    'recipe_engine/file',
    'recipe_engine/path',
]


def RunSteps(api):
    cwd = api.path.cleanup_dir

    with api.context(cwd=cwd):
        # diff_manifests_informational calls _find_root().  Use that to cover
        # both cases in one test.
        api.repo.diff_manifests_informational(
            cwd / 'manifest-internal/snapshot-a.xml',
            cwd / 'manifest-internal/snapshot-b.xml',
        )

    # Next, call _find_root from a subdirectory.  Should not affect
    # context.cwd.
    cwd = api.path.cleanup_dir / 'test' / 'dir' / 'sub'
    api.file.ensure_directory('test dir', cwd)
    expected = str(cwd)
    api.path.mock_add_paths(api.path.cleanup_dir / 'test' / '.repo')
    with api.context(cwd=cwd):
        api.repo._find_root()  # pylint: disable=protected-access
        api.assertions.assertEqual(expected, str(api.context.cwd))


def GenTests(api) -> Generator[recipe_test_api.TestData, None, None]:
    yield api.test('tests')
