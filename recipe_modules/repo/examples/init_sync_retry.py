# -*- coding: utf-8 -*-
# Copyright 2020 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

# Copied from
# https://chromium.googlesource.com/chromiumos/infra/recipes/+/HEAD/recipe_modules/repo/
# pylint: disable=missing-module-docstring

from typing import Generator

from recipe_engine import recipe_test_api

DEPS = [
    'pigweed/repo',
]


def RunSteps(api):
    api.repo.init("https://manifest/url")
    api.repo.sync(attempts=2)


def GenTests(api) -> Generator[recipe_test_api.TestData, None, None]:
    yield (
        api.test('fail-pass')
        + api.step_data('repo sync', retcode=1)
        + api.step_data('repo sync (2)', retcode=0)
    )

    yield (
        api.test('fail-fail', status='INFRA_FAILURE')
        + api.step_data('repo sync', retcode=1)
        + api.step_data('repo sync (2)', retcode=1)
    )
