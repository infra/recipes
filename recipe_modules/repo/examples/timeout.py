# Copyright 2020 The Pigweed Authors
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.
# pylint: disable=missing-module-docstring

from typing import Generator

from recipe_engine import recipe_test_api

DEPS = [
    'pigweed/repo',
]


def RunSteps(api):
    api.repo.init(
        manifest_url='https://foo.googlesource.com',
        manifest_branch='main',
        timeout=15,
        attempts=3,
    )


def GenTests(api) -> Generator[recipe_test_api.TestData, None, None]:
    yield (
        api.test('timeout_persistent', status='INFRA_FAILURE')
        + api.step_data('repo init', times_out_after=20)
        + api.step_data('repo init (2)', times_out_after=20)
        + api.step_data('repo init (3)', times_out_after=20)
    )

    yield (
        api.test('timeout_recover')
        + api.step_data('repo init', times_out_after=20)
        + api.step_data('repo init (2)', times_out_after=20)
    )
