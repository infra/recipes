# Copyright 2024 The Pigweed Authors
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.
"""Test API for copy_roll."""

from __future__ import annotations

from PB.recipe_modules.pigweed.copy_roll.copy_entry import CopyEntry
from recipe_engine import recipe_test_api


class CopyRollTestApi(recipe_test_api.RecipeTestApi):
    """Test API for copy_roll."""

    def entry(
        self,
        dest: str,
        *,
        remote: str | None = None,
        source: str | None = None,
        branch: str = 'main',
    ):
        return CopyEntry(
            remote=remote or 'https://pigweed.googlesource.com/pigweed/pigweed',
            branch=branch,
            source_path=source or dest,
            destination_path=dest,
        )
