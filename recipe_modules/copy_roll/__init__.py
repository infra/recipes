# Copyright 2024 The Pigweed Authors
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.

# pylint: disable=missing-docstring

from __future__ import annotations

DEPS = [
    'fuchsia/git_roll_util',
    'fuchsia/gitiles',
    'fuchsia/roll_commit_message',
    'pigweed/bazel_roll',
    'recipe_engine/file',
    'recipe_engine/path',
    'recipe_engine/step',
]
