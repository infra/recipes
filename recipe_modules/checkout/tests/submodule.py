# Copyright 2020 The Pigweed Authors
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.
"""Full test of submodule functionality of checkout module."""

from __future__ import annotations

from typing import TYPE_CHECKING

from PB.recipe_modules.pigweed.checkout.tests.properties import InputProperties

if TYPE_CHECKING:  # pragma: no cover
    from typing import Generator
    from recipe_engine import recipe_test_api

DEPS = [
    'pigweed/checkout',
    'pigweed/cq_deps',
    'recipe_engine/path',
    'recipe_engine/properties',
    'recipe_engine/step',
]

PROPERTIES = InputProperties


def RunSteps(api, props):  # pylint: disable=invalid-name
    checkout = api.checkout(props.checkout_options)

    with api.step.nest('snapshot_to_dir'):
        checkout.snapshot_to_dir(api.path.start_dir / 'snapshot')


def GenTests(api) -> Generator[recipe_test_api.TestData, None, None]:
    def properties(**kwargs):
        kwargs.setdefault('initialize_submodules', False)
        kwargs.setdefault('remote', api.checkout.pigweed_repo_dot_git)
        kwargs.setdefault('branch', None)
        props = InputProperties(
            checkout_options=api.checkout.git_options(**kwargs)
        )
        return api.properties(props)

    submodule_data = api.checkout.submodules(
        api.checkout.submodule('foo', 'https://x.googlesource.com/foo', '-'),
        api.checkout.submodule('bar', 'https://x.googlesource.com/bar', '+'),
        api.checkout.submodule('baz', 'https://x.googlesource.com/baz.git'),
        # Using 'b/c/d' instead of 'a/b/c' because an 'a/' prefix is treated
        # specially by Gerrit and the buildbucket module.
        api.checkout.submodule(
            'b/c/d', 'https://x.googlesource.com/b/c/d', ' '
        ),
    )

    yield api.test(
        'submodule-try',
        properties(),
        api.checkout.try_test_data(git_repo='https://x.googlesource.com/baz'),
        submodule_data,
        api.checkout.all_changes_applied(),
    )

    yield api.test(
        'submodule-ci',
        properties(),
        api.checkout.ci_test_data(git_repo='https://x.googlesource.com/baz'),
        submodule_data,
    )

    yield api.test(
        'submodule-not-initialized',
        properties(),
        api.checkout.ci_test_data(git_repo='https://x.googlesource.com/b/c/d'),
        submodule_data,
        status='INFRA_FAILURE',
    )

    yield api.test(
        'submodule-try-not-found',
        properties(),
        api.checkout.try_test_data(git_repo='https://x.googlesource.com/xyz'),
        submodule_data,
        api.checkout.no_changes_applied(),
        status='INFRA_FAILURE',
    )

    yield api.test(
        'submodule-try-equivalent',
        properties(
            equivalent_remotes=(
                (
                    'https://z.googlesource.com/foo',
                    'https://x.googlesource.com/foo',
                ),
            ),
        ),
        api.checkout.try_test_data(git_repo='https://z.googlesource.com/foo'),
        submodule_data,
        api.checkout.all_changes_applied(),
    )

    def cl(project, change, patchset=1, name='pigweed'):
        return api.checkout.cl(
            host=f'{name}-review.googlesource.com',
            project=project,
            change=change,
            patchset=patchset,
        )

    yield api.test(
        'submodule-try-multiple',
        properties(),
        api.checkout.try_test_data(
            gerrit_changes=[
                cl('foo', 1234, name='x'),
                cl('bar', 2345, name='x'),
                cl('pigweed/pigweed', 3456),
            ]
        ),
        submodule_data,
        api.checkout.all_changes_applied(),
        api.checkout.change_applied('x:1234'),
        api.checkout.change_applied('x:2345'),
        api.checkout.change_applied('pigweed:3456'),
    )

    yield api.test(
        'submodule-try-multiple-one-missing',
        properties(),
        api.checkout.try_test_data(
            gerrit_changes=[
                cl('foo', 1234, name='x'),
                cl('not-a-submodule', 2345, name='x'),
                cl('pigweed/pigweed', 3456),
            ]
        ),
        submodule_data,
        api.checkout.some_changes_applied(),
        api.checkout.change_applied('x:1234'),
        api.checkout.change_not_applied('x:2345'),
        api.checkout.change_applied('pigweed:3456'),
    )

    # These tests are nominally identical to 'submodule-try-multiple' and
    # 'submodule-try-multiple-one-missing' above but use the cq_deps module.
    # After the 'process gerrit changes' step all changes are minor (e.g.
    # '2345' instead of '2345L').
    yield api.test(
        'submodule-try-multiple-cqdeps',
        properties(),
        api.checkout.try_test_data(
            git_repo='https://x.googlesource.com/foo',
            change_number=1234,
            patch_set=1,
        ),
        api.cq_deps.details(
            'x:1234',
            patches_json=True,
            prefix='checkout pigweed.change data.process gerrit changes.',
        ),
        api.cq_deps.patches_json(
            'x:1234',
            'x:2345',
            'pigweed:3456',
            prefix='checkout pigweed.change data.process gerrit changes.',
        ),
        api.cq_deps.details(
            'x:2345',
            project='bar',
            prefix='checkout pigweed.change data.process gerrit changes.',
        ),
        api.cq_deps.details(
            'pigweed:3456',
            project='pigweed/pigweed',
            prefix='checkout pigweed.change data.process gerrit changes.',
        ),
        submodule_data,
        api.checkout.all_changes_applied(),
        api.checkout.change_applied('x:1234'),
        api.checkout.change_applied('x:2345'),
        api.checkout.change_applied('pigweed:3456'),
    )

    yield api.test(
        'submodule-try-multiple-one-missing-one-forbidden-cqdeps',
        properties(),
        api.checkout.try_test_data(
            git_repo='https://x.googlesource.com/foo',
            change_number=1234,
            patch_set=1,
        ),
        api.cq_deps.details(
            'x:1234',
            patches_json=True,
            prefix='checkout pigweed.change data.process gerrit changes.',
        ),
        api.cq_deps.patches_json(
            'x:1234',
            'x:2345',
            'pigweed:3456',
            'forbidden:9999',
            prefix='checkout pigweed.change data.process gerrit changes.',
        ),
        api.cq_deps.details(
            'x:2345',
            project='not-a-submodule',
            prefix='checkout pigweed.change data.process gerrit changes.',
        ),
        api.cq_deps.details(
            'pigweed:3456',
            project='pigweed/pigweed',
            prefix='checkout pigweed.change data.process gerrit changes.',
        ),
        api.cq_deps.forbidden(
            'forbidden:9999',
            prefix='checkout pigweed.change data.process gerrit changes.',
        ),
        submodule_data,
        api.checkout.some_changes_applied(),
        api.checkout.change_applied('x:1234'),
        api.checkout.change_not_applied('x:2345'),
        api.checkout.change_not_applied('forbidden:9999'),
        api.checkout.change_applied('pigweed:3456'),
    )

    yield api.test(
        'submodule-try-included-excluded',
        properties(included_submodules=['foo'], excluded_submodules=['bar']),
        api.checkout.ci_test_data(),
        status='INFRA_FAILURE',
    )

    yield api.test(
        'submodule-try-included',
        properties(included_submodules=['nanopb']),
        api.checkout.ci_test_data(),
        api.checkout.included_submodule('nanopb'),
    )

    yield api.test(
        'submodule-try-excluded',
        properties(excluded_submodules=['nanopb']),
        api.checkout.ci_test_data(),
        api.checkout.excluded_submodule('nanopb'),
    )

    yield api.test(
        'submodule-try-excluded-missing',
        properties(excluded_submodules=['missing']),
        api.checkout.ci_test_data(),
        status='INFRA_FAILURE',
    )

    yield api.test(
        'submodule-repeated-nomatch',
        properties(),
        api.checkout.try_test_data('https://x.googlesource.com/foo'),
        api.checkout.submodules(
            api.checkout.submodule(
                'foo1',
                'https://x.googlesource.com/foo',
                branch='release',
            ),
            api.checkout.submodule(
                'foo2',
                'https://x.googlesource.com/foo',
                branch='dev',
            ),
        ),
        status='FAILURE',
    )

    yield api.test(
        'submodule-repeated-match',
        properties(),
        api.checkout.try_test_data('https://x.googlesource.com/foo'),
        api.checkout.submodules(
            api.checkout.submodule(
                'foo1',
                'https://x.googlesource.com/foo',
                branch='main',
            ),
            api.checkout.submodule(
                'foo2',
                'https://x.googlesource.com/foo',
                branch='dev',
            ),
        ),
    )

    yield api.test(
        'submodule-repeated-twomatch',
        properties(),
        api.checkout.try_test_data('https://x.googlesource.com/foo'),
        api.checkout.submodules(
            api.checkout.submodule(
                'foo1',
                'https://x.googlesource.com/foo',
                branch='main',
            ),
            api.checkout.submodule(
                'foo2',
                'https://x.googlesource.com/foo',
                branch='main',
            ),
        ),
        status='FAILURE',
    )

    yield api.test(
        'insteadof',
        properties(
            rewrites=[('https://github.com/foo', 'https://github.mirror/foo')],
        ),
        api.checkout.try_test_data(),
        api.checkout.submodules(
            api.checkout.submodule('foobar', 'https://github.com/foo/bar', ' '),
        ),
        api.checkout.all_changes_applied(),
    )
