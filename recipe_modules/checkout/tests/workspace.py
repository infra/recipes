# Copyright 2024 The Pigweed Authors
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.
"""Full test of non-repo non-submodule functionality of checkout module."""

from __future__ import annotations

from typing import TYPE_CHECKING

from PB.recipe_modules.pigweed.checkout.tests.properties import InputProperties

if TYPE_CHECKING:  # pragma: no cover
    from typing import Generator
    from recipe_engine import recipe_test_api

DEPS = [
    'pigweed/checkout',
    'recipe_engine/properties',
    'recipe_engine/step',
]

PROPERTIES = InputProperties


def RunSteps(api, props):  # pylint: disable=invalid-name
    checkout = api.checkout(props.checkout_options)

    def steplog(name, value):  # pylint: disable=invalid-name
        with api.step.nest(name) as pres:
            pres.step_summary_text = repr(value)

    steplog('applied_changes', checkout.applied_changes())


def GenTests(api) -> Generator[recipe_test_api.TestData, None, None]:
    def properties(**kwargs):
        kwargs.setdefault('eligible_workspace_paths', ('WORKSPACE',))
        kwargs.setdefault('remote', 'https://pigweed.googlesource.com/foo')
        kwargs.setdefault('branch', None)
        return api.properties(
            InputProperties(checkout_options=api.checkout.git_options(**kwargs))
        )

    yield api.test(
        'found',
        properties(),
        api.checkout.try_test_data(),
    )

    yield api.test(
        'not-found',
        properties(),
        api.checkout.try_test_data(git_repo='https://x.googlesource.com/bar'),
        status='INFRA_FAILURE',
    )
