# Pigweed Recipes

This repository contains recipes for Pigweed.

A recipe is a Python script that runs a series of commands, using the
[recipe engine](https://chromium.googlesource.com/infra/luci/recipes-py/)
framework from the LUCI project. We use recipes to automatically check
out, build, and test Pigweed and downstream projects using Pigweed in
continuous integration jobs. The commands the recipes use are very similar to
the ones you would use as a developer to check out, build, and test Pigweed in
your local environment.

See go/pigweed-recipe-docs for complete documentation and a guide for getting
started with writing recipes.

## Getting the Code

The recommended way to get the source code is with git.

```sh
git clone https://pigweed.googlesource.com/infra/recipes
```

In most cases you will need a Chromium depot_tools checkout in your `PATH` as
well.

```sh
git clone https://chromium.googlesource.com/chromium/tools/depot_tools ~/depot_tools
echo 'export PATH="$PATH:$HOME/depot_tools"' >> ~/.bashrc
```

## Running Tests

`./presubmit.sh` runs three different sets of tests:

* Recipe expectation tests (`./recipes.py test train`)
* Formatting (`./black --diff --check .`)
* Dependencies (`.recipe_deps/fuchsia/scripts/cleanup_deps.py --check`)

The formatting check will tell you what's wrong but not fix it. For that you
need to run `./black .`. Similarly, the dependencies check will tell you what's
wrong but you'll need to edit the files to fix issues.

If not using `./presubmit.sh` you'll need to run `./scripts/ensure_black.sh`
before `./black` is present.
