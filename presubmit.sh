#!/usr/bin/env bash

set -o errexit
set -o nounset
set -o pipefail
set -o xtrace

# Run recipe tests.
./recipes.py test train

# Check formatting. Do not reformat files.
./scripts/ensure_black.sh
./black --diff --check .

# Check module dependencies. Do not rewrite.
.recipe_deps/fuchsia/scripts/cleanup_deps.py --check
